var flickr = {
	search: function(searchTerm) {
		return $.ajax({
			url: '/flickr.json',
			dataType: 'json'
		});
	}
};

var imdb = {
	search: function(searchTerm) {
		return $.ajax({
			url: '/imdb.json',
			dataType: 'json'
		});
	}
};

function transformResults(flickrResults, imdbResults) {
	var data = [];

	imdbResults.forEach(function(tvShow) {
		var tvShowName = tvShow.name;
		var images = [];

		flickrResults.forEach(function(image) {
			if (tvShowName === image.show) {
				images.push(image);
			}
		});

		data.push({
			name: tvShowName,
			images: images
		});
	});

	return data;
}

$('button').on('click', function() {
	var searchTerm = $('#searchTerm').val();
	var promise1 = flickr.search(searchTerm);
	var promise2 = imdb.search(searchTerm);

	$.when(promise1, promise2).then(function(flickrResults, imdbResults) {
		var data = transformResults(flickrResults[0], imdbResults[0]);
		console.log(data);
	});

});


// $('button').on('click', function() {
// 	var searchTerm = $('#searchTerm').val();
// 	var finishedRequests = 0;
// 	var flickrResults;
// 	var imdbResults;

// 	function handleResults() {
// 		if (finishedRequests === 2) {
// 			// all data has been returned
// 			console.log(flickrResults, imdbResults);
// 		}
// 	}

// 	flickr.search(searchTerm).then(function(response) {
// 		// console.log(response);
// 		finishedRequests++;
// 		flickrResults = response;
// 		handleResults();
// 	});

// 	imdb.search(searchTerm).then(function(response) {
// 		// console.log(response);
// 		finishedRequests++;
// 		imdbResults = response;
// 		handleResults();
// 	});

// });